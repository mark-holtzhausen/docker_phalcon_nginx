#!/bin/bash

clear
/etc/init.d/php5-fpm stop
/etc/init.d/php5-fpm start

echo -e "\n\n=========================================================================================="
echo -e "\t\tPhalcon Server"
echo -e "------------------------------------------------------------------------------------------"
echo -e "\tKill this instance with: \n\n\tdocker stop $(cat /proc/self/cgroup | grep "docker" | sed s/\\//\\n/g | tail -1)\n\n\t\t\tOR\n\n\tdocker kill $(cat /proc/self/cgroup | grep "docker" | sed s/\\//\\n/g | tail -1)"
echo -e "=========================================================================================="
nginx 
