FROM ubuntu:14.04

ENV TERM linux
ENV DEBIAN_FRONTEND noninteractive

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

## Infrastructure
RUN apt-get update -qq && apt-get install -y wget php5-fpm php5-cli nginx nano php5-curl git software-properties-common python-software-properties && \
	add-apt-repository --yes ppa:brightbox/ruby-ng && \
	sudo apt-add-repository ppa:phalcon/stable &&\
	apt-get update -qq && \
	apt-get install -y ruby2.1 ruby2.1-dev make php5-phalcon && \
	gem install sass && \
	wget -O $HOME/composer_install https://getcomposer.org/installer && \
	php $HOME/composer_install --install-dir=/usr/local/bin --filename=composer && rm $HOME/composer_install && \
	apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

## Webserver Configuration
COPY config/phalcon.so /usr/lib/php5/20121212/phalcon.so
COPY config/bin /usr/local/bin
COPY config/nginx /etc/nginx
COPY config/php /etc/php5
COPY site /var/www
RUN chmod +x /usr/local/bin/*.sh && chown -R www-data:www-data /var/www
ENTRYPOINT /usr/local/bin/serve.sh